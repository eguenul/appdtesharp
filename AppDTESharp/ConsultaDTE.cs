using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml; 


public class ConsultaDTE
    {
        public string RutConsultante { get; set; }
        public string DvConsultante { get; set; }
        public string RutCompania { get; set; }
        public string DvCompania { get; set; }
        public string RutReceptor { get; set; }
        public string DvReceptor { get; set; }
        public string TipoDte { get; set; }
        public string FolioDte { get; set; }
        public string FechaEmisionDte { get; set; }
        public string MontoDte { get; set; }
/*
	 public string GetRutConsultante()
        {
            return RutConsultante;
        }

        public void SetRutConsultante(string rutConsultante)
        {
            RutConsultante = rutConsultante;
        }

        public string GetDvConsultante()
        {
            return DvConsultante;
        }

        public void SetDvConsultante(string dvConsultante)
        {
            DvConsultante = dvConsultante;
        }

        public string GetRutCompania()
        {
            return RutCompania;
        }

        public void SetRutCompania(string rutCompania)
        {
            RutCompania = rutCompania;
        }

        public string GetDvCompania()
        {
            return DvCompania;
        }

        public void SetDvCompania(string dvCompania)
        {
            DvCompania = dvCompania;
        }

        public string GetRutReceptor()
        {
            return RutReceptor;
        }

        public void SetRutReceptor(string rutReceptor)
        {
            RutReceptor = rutReceptor;
        }

        public string GetDvReceptor()
        {
            return DvReceptor;
        }

        public void SetDvReceptor(string dvReceptor)
        {
            DvReceptor = dvReceptor;
        }

        public string GetTipoDte()
        {
            return TipoDte;
        }

        public void SetTipoDte(string tipoDte)
        {
            TipoDte = tipoDte;
        }

        public string GetFolioDte()
        {
            return FolioDte;
        }

        public void SetFolioDte(string folioDte)
        {
            FolioDte = folioDte;
        }

        public string GetFechaEmisionDte()
        {
            return FechaEmisionDte;
        }

        public void SetFechaEmisionDte(string fechaEmisionDte)
        {
            FechaEmisionDte = fechaEmisionDte;
        }

        public string GetMontoDte()
        {
            return MontoDte;
        }

        public void SetMontoDte(string montoDte)
        {
            MontoDte = montoDte;
        }
	 
	 
	 
	 */
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
        public string GetEstDte(string login, string clave , string environment)
        {
            
            
            string pathcertificado = login;

            SemillaDTE objsemilla = new SemillaDTE();
            string valorsemilla = objsemilla.GetSeed(environment);

            Token objtoken = new Token();
            string valortoken = objtoken.GetToken(valorsemilla, pathcertificado, clave, "", environment);

            string stringconsulta = $"<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"\n" +
                                    "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"\n" +
                                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                                    "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n" +
                                    "SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">\n" +
                                    "<SOAP-ENV:Body>\n" +
                                    "<m:getEstDte xmlns:m=\"https://{environment}/DTEWS/QueryEstDte.jws\">\n" +
                                    "<RutConsultante>{RutConsultante.Trim()}</RutConsultante>\n" +
                                    "<DvConsultante>{DvConsultante.Trim()}</DvConsultante>\n" +
                                    "<RutCompania>{RutCompania.Trim()}</RutCompania>\n" +
                                    "<DvCompania>{DvCompania.Trim()}</DvCompania>\n" +
                                    "<RutReceptor>{RutReceptor}</RutReceptor>\n" +
                                    "<DvReceptor>{DvReceptor}</DvReceptor>\n" +
                                    "<TipoDte>{TipoDte}</TipoDte>\n" +
                                    "<FolioDte>{FolioDte}</FolioDte>\n" +
                                    "<FechaEmisionDte>{FechaEmisionDte}</FechaEmisionDte>\n" +
                                    "<MontoDte>{MontoDte}</MontoDte>\n" +
                                    "<Token>{valortoken}</Token>\n" +
                                    "</m:getEstDte>\n" +
                                    "</SOAP-ENV:Body>\n" +
                                    "</SOAP-ENV:Envelope>";

            Console.WriteLine(stringconsulta);

            string direccion = $"https://{environment}/DTEWS/QueryEstDte.jws?WSDL";
            WebRequest conexion = WebRequest.Create(direccion);
            conexion.Method = "POST";
            conexion.ContentType = "application/xml; charset=utf-8";
            conexion.Headers.Add("SOAPAction", "getEstDte");

            byte[] byteArray = Encoding.UTF8.GetBytes(stringconsulta);
            conexion.ContentLength = byteArray.Length;

            using (Stream dataStream = conexion.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            using (WebResponse response = conexion.GetResponse())
            using (Stream dataStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(dataStream))
            {
                string salida = reader.ReadToEnd();
                salida = salida.Replace("&lt;", "<").Replace("&quot;", "\"").Replace("&gt;", ">").Replace("&#xd;", "");

                string original = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                string reemplazo = "";
                salida = salida.Replace(original, reemplazo);

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(salida);

                XmlNode estado = doc.SelectSingleNode("//ESTADO");
                return estado.InnerText;
            }
        }

        // Resto del código de las propiedades y métodos
    }