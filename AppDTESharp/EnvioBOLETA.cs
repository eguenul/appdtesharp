using System;
using System.IO;
using System.Xml;
using System.Text;
public class EnvioBOLETA
{
   
    public EnvioBOLETA()
    {
        // Inicializa el XmlDocument en el constructor
     
    }

    public void GeneraEnvio(string aux_tipodte, string nombredte, string rutusuario, string aux_rutemisor, string aux_numresol, string aux_fecharesol)
    {
        try{
       
            // Usa el mismo documento docENV para todas las operaciones
      
       XmlDocument doc = new XmlDocument();
            
       string xmlDeclaration = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
       string envioBoletaOpeningTag = "<EnvioBOLETA xmlns=\"http://www.sii.cl/SiiDte\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"1.0\" xsi:schemaLocation=\"http://www.sii.cl/SiiDte EnvioBOLETA_v11.xsd\">";
       string envioBoletaClosingTag = "</EnvioBOLETA>";

            
            
        // Crea el XmlDocument e carga la declaración y el nodo principal
        doc.LoadXml(xmlDeclaration + envioBoletaOpeningTag + envioBoletaClosingTag);

         XmlNode root = doc.DocumentElement;
   
            XmlNode setdte = doc.CreateElement("SetDTE");
            XmlAttribute attrIdSetDTE = doc.CreateAttribute("ID");
            attrIdSetDTE.Value = "SetDoc";
            setdte.Attributes.Append(attrIdSetDTE);
            root.AppendChild(setdte);

            
            XmlNode caratula = doc.CreateElement("Caratula");
            XmlAttribute attrVersionCaratula = doc.CreateAttribute("version");
            attrVersionCaratula.Value = "1.0";
            caratula.Attributes.Append(attrVersionCaratula);
            setdte.AppendChild(caratula);
       
          
            
            
            
            XmlNode rutemisor = doc.CreateElement("RutEmisor");
            rutemisor.InnerText = aux_rutemisor.Trim();
            caratula.AppendChild(rutemisor);

            XmlNode rutenvia = doc.CreateElement("RutEnvia");
            rutenvia.InnerText = rutusuario.Trim();
            caratula.AppendChild(rutenvia);

            XmlNode rutreceptor = doc.CreateElement("RutReceptor");
            rutreceptor.InnerText = "60803000-K";
            caratula.AppendChild(rutreceptor);

            XmlNode fecharesol = doc.CreateElement("FchResol");
            fecharesol.InnerText = aux_fecharesol;
            caratula.AppendChild(fecharesol);

            XmlNode nroresol = doc.CreateElement("NroResol");
            nroresol.InnerText = aux_numresol;
            caratula.AppendChild(nroresol);

            DateTime date = DateTime.Now;
            string stringFecha = date.ToString("yyyy-MM-dd");
            string stringHora = date.ToString("THH:mm:ss");

            XmlNode tmstfirmaenv = doc.CreateElement("TmstFirmaEnv");
            tmstfirmaenv.InnerText = stringFecha + stringHora;
            caratula.AppendChild(tmstfirmaenv);

            XmlNode subtotdte = doc.CreateElement("SubTotDTE");

            XmlNode tpodte = doc.CreateElement("TpoDTE");
            tpodte.InnerText = aux_tipodte;
            subtotdte.AppendChild(tpodte);

            XmlNode nrodte = doc.CreateElement("NroDTE");
            nrodte.InnerText = "1";
            subtotdte.AppendChild(nrodte);

            caratula.AppendChild(subtotdte);
            
            

            // Guarda el XML con indentación en una sola columna
       
		  XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "", // Cambiado a cadena vacía para indentar en una sola columna
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace,
                Encoding = Encoding.GetEncoding("ISO-8859-1")    
            };

            using (XmlWriter writer = XmlWriter.Create("" + "ENV" + nombredte, settings))
            {
                doc.Save(writer);
            }
        
            
            
		} 

        
        
            catch (Exception ex)
        {
            Console.WriteLine("Error en GeneraEnvio: " + ex.Message);
        }
    

	
}

	
   public void addBOLETA(string nombredte)
        {
    
 string archivo = nombredte;

    using (FileStream archivodte = new FileStream(archivo, FileMode.Open))
    using (StreamReader inputdte = new StreamReader(archivodte, Encoding.GetEncoding("ISO-8859-1")))
    {
        string sourcedte = inputdte.ReadToEnd();

        XmlDocument docENV = new XmlDocument();
        string filepath = "ENV" + nombredte;
        docENV.Load(filepath);

        XmlElement setdte = (XmlElement)docENV.GetElementsByTagName("SetDTE")[0];
        
           XmlAttribute attrIdSetDTE = docENV.CreateAttribute("xmlns");
            attrIdSetDTE.Value = "http://www.sii.cl/SiiDte";
            setdte.Attributes.Append(attrIdSetDTE);
            
        
        
        
        
        
        
        XmlDocument doc2 = new XmlDocument();
        doc2.PreserveWhitespace = true;
        doc2.LoadXml(sourcedte);

bool shouldImport = true; 
   if (shouldImport)
{
    XmlNode fragmentNode = docENV.ImportNode(doc2.DocumentElement, true);
    setdte.AppendChild(fragmentNode);
    docENV.Save("ENV" + nombredte);
}
        
        
    }
  
   
   }


}	
	
	
	
	




