using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;



public class FirmaDigital
{
    private static string certPath = "ruta_del_certificado.pfx";
    private static string certPassword = "password_del_certificado";

    public static string FirmarDocumento(string documentoPath, string elementoId)
    {
        try
        {
            XmlDocument doc = new XmlDocument();
            doc.PreserveWhitespace = true;
            doc.Load(documentoPath);

            X509Certificate2 cert = new X509Certificate2(certPath, certPassword);

            SignedXml signedXml = new SignedXml(doc);
            signedXml.SigningKey = cert.PrivateKey;

            Reference reference = new Reference($"#{elementoId}");
            reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
            signedXml.AddReference(reference);

            KeyInfo keyInfo = new KeyInfo();
            keyInfo.AddClause(new KeyInfoX509Data(cert));
            signedXml.KeyInfo = keyInfo;

            signedXml.ComputeSignature();
            XmlElement signatureElement = signedXml.GetXml();
            XmlNodeList elements = doc.GetElementsByTagName(elementoId);
            elements[0].AppendChild(doc.ImportNode(signatureElement, true));

            return doc.OuterXml; // Retorna el documento como cadena con la firma
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error al firmar el documento: {ex.Message}");
            return null;
        }
    }

    public  void MainFirma()
    {
        // Rutas de los documentos XML
        string dtePath = "ruta_del_DTE.xml";
        string envioBoletaPath = "ruta_del_EnvioBOLETA.xml";

        // ID del elemento a firmar en cada documento
        string dteElementId = "F1T39";
        string envioBoletaElementId = "SetDoc";

        // Firmar el DTE y guardarlo como cadena
        string dteFirmado = FirmarDocumento(dtePath, dteElementId);

        if (!string.IsNullOrEmpty(dteFirmado))
        {
            try
            {
                // Crear el documento EnvioBOLETA
                XmlDocument docEnvioBoleta = new XmlDocument();
                docEnvioBoleta.Load(envioBoletaPath);

                // Importar el DTE firmado en el documento EnvioBOLETA
                XmlDocumentFragment fragmentNode = docEnvioBoleta.CreateDocumentFragment();
                fragmentNode.InnerXml = dteFirmado;

                XmlNode setDteNode = docEnvioBoleta.SelectSingleNode($"/EnvioBOLETA/{envioBoletaElementId}");
                setDteNode.AppendChild(fragmentNode);

                // Guardar el documento EnvioBOLETA
                docEnvioBoleta.Save("ruta_del_EnvioBOLETA_Firmado.xml");

                Console.WriteLine("Documento EnvioBOLETA firmado correctamente.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al agregar el DTE firmado al EnvioBOLETA: {ex.Message}");
            }
        }
    }
}