public class Detalle{

private int nrolinea;
private String tpocodigo;
private String vlrcodigo;

private String nmbitem;
private String dscitem;
private int qtyitem;
private int prcitem;
private int montoitem;
private int descuentopct;
private int descuentomonto;
private int indexe;
private int codimpadic;




public int getCodimpadic(){
	return this.codimpadic;
}

public void setCodimpadic(int codimpadic){
	this.codimpadic = codimpadic;

}




	public int getNrolinea() {
		return this.nrolinea;
	}

	public void setNrolinea(int nrolinea) {
		this.nrolinea = nrolinea;
	}

	public String getTpocodigo() {
		return this.tpocodigo;
	}

	public void setTpocodigo(String tpocodigo) {
		this.tpocodigo = tpocodigo;
	}

	public String getVlrcodigo() {
		return this.vlrcodigo;
	}

	public void setVlrcodigo(String vlrcodigo) {
		this.vlrcodigo = vlrcodigo;
	}

	public String getNmbitem() {
		return this.nmbitem;
	}

	public void setNmbitem(String nmbitem) {
		this.nmbitem = nmbitem;
	}

	public String getDscitem() {
		return this.dscitem;
	}

	public void setDscitem(String dscitem) {
		this.dscitem = dscitem;
	}

	public int getQtyitem() {
		return this.qtyitem;
	}

	public void setQtyitem(int qtyitem) {
		this.qtyitem = qtyitem;
	}

	public int getPrcitem() {
		return this.prcitem;
	}

	public void setPrcitem(int prcitem) {
		this.prcitem = prcitem;
	}

	public int getMontoitem() {
		return this.montoitem;
	}

	public void setMontoitem(int montoitem) {
		this.montoitem = montoitem;
	}

	public int getDescuentopct() {
		return this.descuentopct;
	}

	public void setDescuentopct(int descuentopct) {
		this.descuentopct = descuentopct;
	}

	public int getDescuentomonto() {
		return this.descuentomonto;
	}

	public void setDescuentomonto(int descuentomonto) {
		this.descuentomonto = descuentomonto;
	}

	public int getIndexe() {
		return this.indexe;
	}

	public void setIndexe(int indexe) {
		this.indexe = indexe;
	}


}