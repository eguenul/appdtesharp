using System;
using System.Xml;

public class xmlDTEBOLETA{





public void crearXML(DteModel objDTE ,String pathdte){

    XmlDocument doc = new XmlDocument();
    doc.LoadXml("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><DTE version=\"1.0\">" +"</DTE>");

    XmlNode root = doc.DocumentElement;

    //Create a new node.
    XmlElement documento = doc.CreateElement("Documento");
    

    //ENCABEZADO DEL XML
    root.AppendChild(documento);
    XmlElement encabezado = doc.CreateElement("Encabezado");
    documento.AppendChild(encabezado);
    
  //IDENTIFICACION DEL TIPO DE DOCUMENTO
     IdDTE objIdDTE = objDTE.getIddte();


    XmlElement iddoc = doc.CreateElement("IdDoc");

    XmlElement tipodte = doc.CreateElement("TipoDTE");
    tipodte.InnerText = objIdDTE.getTipodte().ToString();
    
    XmlElement folio = doc.CreateElement("Folio");
    folio.InnerText = objIdDTE.getNumdte().ToString();
    
    XmlElement fchemis = doc.CreateElement("FchEmis");
    fchemis.InnerText = objIdDTE.getFechaemision();



   documento.SetAttribute("ID","F"+objIdDTE.getNumdte().ToString()+"T"+objIdDTE.getTipodte().ToString());



    /* <IndServicio>3</IndServicio>

    */
 iddoc.AppendChild(tipodte);
 iddoc.AppendChild(folio);
 iddoc.AppendChild(fchemis);

    
 if(objIdDTE.getIndservicio()!=0 ){  
    XmlElement indservicio = doc.CreateElement("IndServicio");
    indservicio.InnerText = objIdDTE.getIndservicio().ToString(); 
    iddoc.AppendChild(indservicio);
 }


    
 if(objIdDTE.getIndmntneto()!=0 ){  
    XmlElement indmntneto = doc.CreateElement("IndMntNeto");
    indmntneto.InnerText = objIdDTE.getIndmntneto().ToString(); 
    iddoc.AppendChild(indmntneto);
 }

/*
      
      if(!"0".equals(encabezadodte.getIndmntneto()) && encabezadodte.getIndmntneto()!=null ){ 
                
                Element indmntneto = this.doc.createElement("IndMntNeto");
                indmntneto.setTextContent(encabezadodte.getIndmntneto());
                iddoc.appendChild(indmntneto);
      }

*/




/*
   if(objIdDTE.getTipodespacho()!=0){
       XmlElement tipodespacho = doc.CreateElement("TipoDespacho");
      tipodespacho.InnerText = objIdDTE.getTipodespacho().ToString();
      iddoc.AppendChild(tipodespacho);
   }
    
   
   if(objIdDTE.getIndtraslado()!=0){
       XmlElement indtraslado = doc.CreateElement("IndTraslado");
       indtraslado.InnerText = objIdDTE.getIndtraslado().ToString();
      iddoc.AppendChild(indtraslado);
   }
*/



//DATOS DEL EMISOR EN EL XML
Emisor objEmisor = objDTE.getEmisor();
XmlElement emisor = doc.CreateElement("Emisor");
XmlElement rutemisor = doc.CreateElement("RUTEmisor");
rutemisor.InnerText = objEmisor.getRutemisor();
XmlElement rznsoc = doc.CreateElement("RznSocEmisor");
rznsoc.InnerText = objEmisor.getRsemisor();
XmlElement giroemis = doc.CreateElement("GiroEmisor");

giroemis.InnerText = objEmisor.getGiroemisor();

/*
XmlElement telefono = doc.CreateElement("Telefono");
*/

XmlElement dirorigen = doc.CreateElement("DirOrigen");
dirorigen.InnerText = objEmisor.getDiremisor();
XmlElement cmnaorigen = doc.CreateElement("CmnaOrigen");
cmnaorigen.InnerText = objEmisor.getCmnaemisor();   
XmlElement ciudadorigen = doc.CreateElement("CiudadOrigen");   
ciudadorigen.InnerText = objEmisor.getCiuemisor();

emisor.AppendChild(rutemisor);
emisor.AppendChild(rznsoc);

emisor.AppendChild(giroemis);


/*
emisor.AppendChild(telefono);
*/
emisor.AppendChild(dirorigen);
emisor.AppendChild(cmnaorigen);
emisor.AppendChild(ciudadorigen);

// DATOS DEL RECEPTOR EN EL XML
Receptor objReceptor = objDTE.getReceptor();
XmlElement receptor = doc.CreateElement("Receptor");
XmlElement rutrecept = doc.CreateElement("RUTRecep");
rutrecept.InnerText = objReceptor.getRutreceptor();
XmlElement rznsocrecep = doc.CreateElement("RznSocRecep");
rznsocrecep.InnerText = objReceptor.getRsreceptor();
/*
XmlElement girorecep = doc.CreateElement("GiroRecep");
girorecep.InnerText = objReceptor.getGiroreceptor();
XmlElement dirrecep = doc.CreateElement("DirRecep");
dirrecep.InnerText = objReceptor.getDirreceptor();
XmlElement cmnarecep = doc.CreateElement("CmnaRecep");
cmnarecep.InnerText = objReceptor.getCmnareceptor();
XmlElement ciudadrecep = doc.CreateElement("CiudadRecep");
ciudadrecep.InnerText = objReceptor.getCiureceptor();
*/
receptor.AppendChild(rutrecept);
receptor.AppendChild(rznsocrecep);
/*
receptor.AppendChild(girorecep);
receptor.AppendChild(dirrecep);
receptor.AppendChild(cmnarecep);
receptor.AppendChild(ciudadrecep);
*/

encabezado.AppendChild(iddoc);
encabezado.AppendChild(emisor);    
encabezado.AppendChild(receptor);


//SECCION DE LOS TOTALES
Totales objTotales = objDTE.getTotales();



XmlElement totales = doc.CreateElement("Totales");

if(objTotales.getMontoneto()!=0){
XmlElement mntneto = doc.CreateElement("MntNeto");
mntneto.InnerText = objTotales.getMontoneto().ToString();
totales.AppendChild(mntneto);
}

if(objTotales.getMontoexento()!=0){
XmlElement mntexe = doc.CreateElement("MntExe");
mntexe.InnerText = objTotales.getMontoexento().ToString();
totales.AppendChild(mntexe);
}





if(objTotales.getMontoiva()!=0){

XmlElement iva = doc.CreateElement("IVA");
iva.InnerText = objTotales.getMontoiva().ToString();
totales.AppendChild(iva);
}

XmlElement mnttotal= doc.CreateElement("MntTotal");
mnttotal.InnerText = objTotales.getMontototal().ToString();




totales.AppendChild(mnttotal);
encabezado.AppendChild(totales);



// inicializo los detalles

List<Detalle> listadetalle = objDTE.getDetalle();


foreach (Detalle x in listadetalle)
        {
            int nrolinea = x.getNrolinea();
            String aux = x.getNmbitem();
            int auxprcitem = x.getPrcitem();
            int aux_montotiem = x.getMontoitem();
            int cantidad = x.getQtyitem();
            String tpo_codigo = x.getTpocodigo();
            String vlr_codigo = x.getVlrcodigo();
          

            XmlElement detalle = doc.CreateElement("Detalle");
            XmlElement nrolindet = doc.CreateElement("NroLinDet");
            XmlElement cdgitem = doc.CreateElement("CdgItem");
            XmlElement tpocodigo = doc.CreateElement("TpoCodigo");
            tpocodigo.InnerText = tpo_codigo;
            XmlElement vlrcodigo = doc.CreateElement("VlrCodigo");
            vlrcodigo.InnerText = vlr_codigo;
            cdgitem.AppendChild(tpocodigo);
            cdgitem.AppendChild(vlrcodigo);

          
            XmlElement nmbitem = doc.CreateElement("NmbItem");
            nmbitem.InnerText = aux;
           
            XmlElement qtyitem = doc.CreateElement("QtyItem");
            qtyitem.InnerText = cantidad.ToString();
           
            XmlElement prcitem = doc.CreateElement("PrcItem");
            prcitem.InnerText = auxprcitem.ToString();
            
            XmlElement montoitem = doc.CreateElement("MontoItem");
            montoitem.InnerText = aux_montotiem.ToString();
            


            nrolindet.InnerText = nrolinea.ToString();

            detalle.AppendChild(nrolindet);
            detalle.AppendChild(cdgitem);
            detalle.AppendChild(nmbitem);
            detalle.AppendChild(qtyitem);
            detalle.AppendChild(prcitem);

            if(x.getDescuentopct()!=0){
                   XmlElement descuentopct = doc.CreateElement("DescuentoPct");
                   descuentopct.InnerText = x.getDescuentopct().ToString();
                  detalle.AppendChild(descuentopct);
            }
            if(x.getDescuentomonto()!=0){
                   XmlElement descuentomonto = doc.CreateElement("DescuentoMonto");
                   descuentomonto.InnerText = x.getDescuentomonto().ToString();
                  detalle.AppendChild(descuentomonto);
            }

            

            detalle.AppendChild(montoitem);
            documento.AppendChild(detalle);



      // EN CASO DE EXISTIR DESCUENTOS GLOBALES

         try{

         List<DscRcgGlobal> listdscrcgglobal = objDTE.getDscrcgglobal();

         foreach (DscRcgGlobal y in listdscrcgglobal){
            XmlElement dscrcgglobal = doc.CreateElement("DscRcgGlobal");
           
            XmlElement nrolindr = doc.CreateElement("NroLinDR");
            nrolindr.InnerText = y.getNrolindr().ToString();
            dscrcgglobal.AppendChild(nrolindr);
            
            XmlElement tpomov = doc.CreateElement("TpoMov");
            tpomov.InnerText = y.getTpomov();
            dscrcgglobal.AppendChild(tpomov);   

            
            XmlElement glosadr = doc.CreateElement("GlosaDR");
            glosadr.InnerText = y.getGlosaDr();
            dscrcgglobal.AppendChild(glosadr);   

            XmlElement tpovalor = doc.CreateElement("TpoValor");
            tpovalor.InnerText = y.getTpovalor();
            dscrcgglobal.AppendChild(tpovalor);   
   
            XmlElement valordr = doc.CreateElement("ValorDR");
            valordr.InnerText = y.getValordr().ToString();
            dscrcgglobal.AppendChild(valordr);   
   





            documento.AppendChild(dscrcgglobal);
            

         
         
         
            }
         
         }catch(Exception e){
             Console.Write("NO HAY DESCUENTOS GLOBALES PARA AGREGAR");  
         }





/*
 try{
  List<Referencia> listreferencia = objDTE.getReferencia();

       foreach (Referencia y in listreferencia){
           XmlElement referencia = doc.CreateElement("Referencia");   
           XmlElement nrolinref = doc.CreateElement("NroLinRef");   
           nrolinref.InnerText = y.getNrolinref().ToString();
           XmlElement tpodocref = doc.CreateElement("TpoDocRef");   
           tpodocref.InnerText = y.getTpodocref();
           XmlElement folioref = doc.CreateElement("FolioRef");   
           folioref.InnerText = y.getFolioref().ToString();
           XmlElement fchref = doc.CreateElement("FchRef");   
           fchref.InnerText = y.getFchref();

         
           XmlElement razonref = doc.CreateElement("RazonRef");   
           razonref.InnerText = y.getRazonref();
            


           referencia.AppendChild(nrolinref);
           referencia.AppendChild(tpodocref);
           referencia.AppendChild(folioref);
           referencia.AppendChild(fchref);
           
            if (y.getCodref()!=0){
               XmlElement codref = doc.CreateElement("CodRef");   
                  codref.InnerText = y.getCodref().ToString();
              
                    referencia.AppendChild(codref);
            }
          
          referencia.AppendChild(razonref);
          documento.AppendChild(referencia);
       }   
    }catch (Exception e)
     {
  
         Console.Write(e.Message);

     }

     */

XmlWriterSettings settings = new XmlWriterSettings
{
    Indent = true,
    IndentChars = "", // Cambiado a cadena vac�a para indentar en una sola columna
    NewLineChars = "\r\n",
    NewLineHandling = NewLineHandling.Replace,
	Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1"),	
};

using (XmlWriter writer = XmlWriter.Create(pathdte, settings))
{
    doc.Save(writer);
}	
	
	

}  
}	
}