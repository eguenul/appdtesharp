using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;
public class Timbre{	
	
        public string RutEmisor { get; set; }
        public string RazonSocial { get; set; }
        public string TipoDocumento { get; set; }
        public string Desde { get; set; }
        public string Hasta { get; set; }
        public string Fecha { get; set; }
        public string TextoE { get; set; }
        public string TextoM { get; set; }
        public string TextoIdk { get; set; }
        public string TextoFrma { get; set; }

	
	
	
	
	
 public void CreaTimbre(string nombreDte, string pathCaf, string parametroRut)
        {
            try
            {
                System.Text.Encoding iso_8859_1 = System.Text.Encoding.GetEncoding("ISO-8859-1");

                string filePath =  nombreDte;
                XmlDocument doc = new XmlDocument();
                doc.PreserveWhitespace = false;
                doc.Load(filePath);

                XmlNode documento = doc.SelectSingleNode("//Documento");
                XmlElement ted = doc.CreateElement("TED");
                ted.SetAttribute("version", "1.0");
                documento.AppendChild(ted);

                DateTime date = DateTime.Now;
                string stringFecha = date.ToString("yyyy-MM-dd");
                string stringHora = date.ToString("HH:mm:ss");

                string rsakey = LeerCaf(parametroRut, doc.SelectSingleNode("//TipoDTE").InnerText, pathCaf, nombreDte);

                string strDigest = "<DD><RE>" + doc.SelectSingleNode("//RUTEmisor").InnerText + "</RE>"
                    + "<TD>" + doc.SelectSingleNode("//TipoDTE").InnerText + "</TD>"
                    + "<F>" + doc.SelectSingleNode("//Folio").InnerText + "</F>"
                    + "<FE>" + doc.SelectSingleNode("//FchEmis").InnerText + "</FE>"
                    + "<RR>" + doc.SelectSingleNode("//RUTRecep").InnerText + "</RR>"
                    + "<RSR>" + doc.SelectSingleNode("//RznSocRecep").InnerText + "</RSR>"
                    + "<MNT>" + doc.SelectSingleNode("//MntTotal").InnerText + "</MNT>"
                    + "<IT1>" + doc.SelectSingleNode("//NmbItem").InnerText + "</IT1>"
                    + "<CAF version=\"1.0\">"
                    + "<DA><RE>" + this.RutEmisor + "</RE>"
                    + "<RS>" + FormatoTimbre(this.RazonSocial) + "</RS>"
                    + "<TD>" + this.TipoDocumento + "</TD>"
                    + "<RNG>" + "<D>" + this.Desde + "</D>" + "<H>" + this.Hasta + "</H>" + "</RNG>"
                    + "<FA>" + this.Fecha + "</FA>"
                    + "<RSAPK><M>" + TextoM + "</M>"
                    + "<E>" + TextoE + "</E></RSAPK>"
                    + "<IDK>" + TextoIdk + "</IDK></DA>"
                    + "<FRMA algoritmo=\"SHA1withRSA\">" + TextoFrma + "</FRMA></CAF>"
                    + "<TSTED>" + stringFecha + "T" + stringHora + "</TSTED></DD>";

                XmlNode nodeDD = doc.CreateDocumentFragment();
                nodeDD.InnerXml = strDigest;
                ted.AppendChild(nodeDD);
            
								
                string contenidoTimbre = SignTimbre(strDigest, rsakey);
     
				Console.WriteLine(strDigest);
				
				Console.WriteLine(contenidoTimbre);
				
				
				
				

                XmlElement frmt = doc.CreateElement("FRMT");
                frmt.SetAttribute("algoritmo", "SHA1withRSA");
                frmt.InnerText = contenidoTimbre;

                ted.AppendChild(frmt);

                XmlElement tmstFirma = doc.CreateElement("TmstFirma");
                tmstFirma.InnerText = stringFecha + "T" + stringHora;
                documento.AppendChild(tmstFirma);

                string outputFilePath = nombreDte;
                doc.Save(outputFilePath);

                Console.WriteLine("Done");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error al crear el timbre: {ex.Message}");
            }
        }

        public string LeerCaf(string auxRutEmisor, string tipoDocumento, string pathCaf,  string nombreDte)
        {
            string filePath2 = pathCaf+$"F{auxRutEmisor}T{tipoDocumento}.xml";

            XmlDocument doc2 = new XmlDocument();
            doc2.Load(filePath2);

            RutEmisor = doc2.SelectSingleNode("//RE").InnerText;
            RazonSocial = doc2.SelectSingleNode("//RS").InnerText;
            TipoDocumento = doc2.SelectSingleNode("//TD").InnerText;
            Desde = doc2.SelectSingleNode("//D").InnerText;
            Hasta = doc2.SelectSingleNode("//H").InnerText;
            Fecha = doc2.SelectSingleNode("//FA").InnerText;
            TextoE = doc2.SelectSingleNode("//E").InnerText;
            TextoM = doc2.SelectSingleNode("//M").InnerText;
            TextoIdk = doc2.SelectSingleNode("//IDK").InnerText;
            TextoFrma = doc2.SelectSingleNode("//FRMA").InnerText;

            string stringRsask = doc2.SelectSingleNode("//RSASK").InnerText;
            
          /*  string nombreArchivo = nombreDte + ".rsa";
            File.WriteAllText(nombreArchivo, stringRsask, Encoding.GetEncoding("ISO-8859-1"));
           */ 
			return stringRsask;
			
        }

        private string FormatoTimbre(string cadena)
        {
            return cadena.Replace("&", "&amp;");
        }
    
	
	
public string SignTimbre(string mensaje, string privateKey)
{
    try
    {
        Console.WriteLine(mensaje);
		
		privateKey = privateKey.Replace("-----BEGIN RSA PRIVATE KEY-----","");
		
		privateKey = privateKey.Replace("-----END RSA PRIVATE KEY-----","");
		
        
        // Leer el contenido del archivo de la clave privada
		/*
        string privateKey = File.ReadAllText(privateKeyFilePath);
*/
        RSACryptoServiceProvider rsaProvider = new RSACryptoServiceProvider();

        // Limpiar la clave privada antes de usarla
        byte[] privateKeyBytes = Convert.FromBase64String(privateKey);

        rsaProvider.ImportRSAPrivateKey(privateKeyBytes, out _);

        byte[] data = Encoding.GetEncoding("ISO-8859-1").GetBytes(mensaje);

        byte[] resultado = rsaProvider.SignData(data, new SHA1CryptoServiceProvider());
        string encoded = Convert.ToBase64String(resultado);

        Console.WriteLine(encoded);
        return encoded;
    }
    catch (Exception ex)
    {
        Console.WriteLine($"Error al firmar el timbre: {ex.Message}");
        return string.Empty;
    }
}


}	
	
	