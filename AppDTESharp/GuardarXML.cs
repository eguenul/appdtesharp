using System;
using System.Xml;

public class GuardarXML
{
   public void grabarContenido(string contenido, string nombreArchivo)
    {
        try
        {
            // Crear un nuevo XmlWriter sin indentación
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = false,
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace,
				OmitXmlDeclaration = true	
            };

            using (XmlWriter writer = XmlWriter.Create(nombreArchivo, settings))
            {
                // Escribir el contenido directamente en el archivo
                writer.WriteRaw(contenido);
            }

            Console.WriteLine($"String guardado como XML en {nombreArchivo}");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error al guardar el string como XML: {ex.Message}");
        }
    }
	
}	