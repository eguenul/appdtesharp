
public class DscRcgGlobal{

    private int nrolindr;
    private String tpomov;
    private String GlosaDr;
    private String tpovalor;
    private int valordr;


    public int getNrolindr()
    {
        return this.nrolindr;
    }

    public void setNrolindr(int nrolindr)
    {
        this.nrolindr = nrolindr;
    }

    public String getTpomov()
    {
        return this.tpomov;
    }

    public void setTpomov(String tpomov)
    {
        this.tpomov = tpomov;
    }

    public String getGlosaDr()
    {
        return this.GlosaDr;
    }

    public void setGlosaDr(String GlosaDr)
    {
        this.GlosaDr = GlosaDr;
    }

    public String getTpovalor()
    {
        return this.tpovalor;
    }

    public void setTpovalor(String tpovalor)
    {
        this.tpovalor = tpovalor;
    }

    public int getValordr()
    {
        return this.valordr;
    }

    public void setValordr(int valordr)
    {
        this.valordr = valordr;
    }




}