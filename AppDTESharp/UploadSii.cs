using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

    public class UploadSii
    {
        private string urlenvironment;

        public UploadSii(string urlenvironment)
        {
            this.urlenvironment = urlenvironment;
        }

        public string UploadSiiFile(string valortoken, string pathdte, string nombredte, string rutemisor, string rutusuario)
        {
            string[] arrayrutemisor = rutemisor.Split('-');
            string[] arrayrutusuario = rutusuario.Split('-');

        

            string archivo = "ENV"+nombredte ;

            try
            {
                using (WebClient client = new WebClient())
                {
                    client.Headers.Add("Content-Type", "multipart/form-data; boundary=9022632e1130lc4");
                    client.Headers.Add("User-Agent", "Mozilla/4.0 (compatible; PROG 1.0; Windows NT 5.0; YComp 5.0.2.4)");
                    client.Headers.Add("Cookie", "TOKEN=" + valortoken);

                    string contenido = File.ReadAllText(archivo);

                    string stringRequest =
                        "--9022632e1130lc4" + "\r\n" +
                        "Content-Disposition: form-data;" + " name=" + "\"" + "rutSender" + "\"" + "\r\n" +
                        "\r\n" + arrayrutusuario[0] + "\r\n" +
                        "--9022632e1130lc4" + "\r\n" +
                        "Content-Disposition: form-data; name=" + "\"" + "dvSender" + "\"" + "\r\n" +
                        "\r\n" + arrayrutusuario[1] + "\r\n" +
                        "--9022632e1130lc4" + "\r\n" +
                        "Content-Disposition: form-data; name=" + "\"" + "rutCompany" + "\"" +
                        "\r\n" + "\r\n" + arrayrutemisor[0] + "\r\n" +
                        "--9022632e1130lc4" + "\r\n" +
                        "Content-Disposition: form-data; name=" + "\"" + "dvCompany" + "\"" +
                        "\r\n" + "\r\n" +
                        arrayrutemisor[1] + "\r\n" +
                        "--9022632e1130lc4" + "\r\n" +
                        "Content-Disposition: form-data; name=" + "\r\n" + "archivo" + "\""+ ";filename=" + "\"" + archivo + "\"" + "\r\n" +
                        "Content-Type: application/octet-stream" + "\r\n" +
                        "Content-Transfer-Encoding: binary" + "\r\n" +
                        "\r\n" +
                        contenido + "\r\n" + "\r\n" + "--9022632e1130lc4--";

                    Console.Write(stringRequest);

                    byte[] responseArray = client.UploadData("https://" + this.urlenvironment + "/cgi_dte/UPL/DTEUpload", "POST", Encoding.UTF8.GetBytes(stringRequest));
                    string targetString = Encoding.UTF8.GetString(responseArray);

                    return ReadTrackId(targetString);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;
            }
        }

        private string ReadTrackId(string targetString)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(targetString);

                XmlNode nl = doc.SelectSingleNode("//TRACKID");
                string valortrackid = nl.InnerText;

                return valortrackid;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error reading TRACKID: " + ex.Message);
                return null;
            }
        }
    }
