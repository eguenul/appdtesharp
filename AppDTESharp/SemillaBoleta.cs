using System;
using System.IO;
using System.Net;
using System.Xml;

public class SemillaBOLETA
{
    public string GetSeed()
    {
        string apiUrl = "https://apicert.sii.cl/recursos/v1/boleta.electronica.semilla";

        using (WebClient client = new WebClient())
        {
            try
            {
                string result = client.DownloadString(apiUrl);
                return ReadSeed(result);
            }
            catch (Exception ex)
            {
                // Manejar excepciones específicas según tus necesidades
                Console.WriteLine($"Error al obtener la semilla: {ex.Message}");
                return null;
            }
        }
    }

    public string ReadSeed(string request)
    {
        try
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(request);

            XmlNodeList nl = doc.GetElementsByTagName("SEMILLA");
            XmlElement el = (XmlElement)nl.Item(0);
            string valorsemilla = el?.FirstChild?.InnerText;
            Console.WriteLine(valorsemilla);
            return valorsemilla;
        }
        catch (Exception ex)
        {
            // Manejar excepciones específicas según tus necesidades
            Console.WriteLine($"Error al leer la semilla: {ex.Message}");
            return null;
        }
    }
}
