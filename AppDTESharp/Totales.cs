  public class Totales{
   private int montoexento;
   private int montoiva;
   private int montototal;
   private int tasaiva;
   private int montoneto;


	public int getMontoexento() {
		return this.montoexento;
	}

	public void setMontoexento(int montoexento) {
		this.montoexento = montoexento;
	}

	public int getMontoiva() {
		return this.montoiva;
	}

	public void setMontoiva(int montoiva) {
		this.montoiva = montoiva;
	}

	public int getMontototal() {
		return this.montototal;
	}

	public void setMontototal(int montototal) {
		this.montototal = montototal;
	}

	public int getTasaiva() {
		return this.tasaiva;
	}

	public void setTasaiva(int tasaiva) {
		this.tasaiva = tasaiva;
	}

	public int getMontoneto() {
		return this.montoneto;
	}

	public void setMontoneto(int montoneto) {
		this.montoneto = montoneto;
	}

  }