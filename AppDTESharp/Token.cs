using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
   public class Token
    {
        
       public Token(){
		   
	   } 
       

        public string GetToken(string valorsemilla, string pathcertificado, string clave, string nombredte, string environment)
        {
            valorsemilla = valorsemilla.Replace("^0*", "");
            long cadenaResultadoInt = long.Parse(valorsemilla);
            string valuesemilla = cadenaResultadoInt.ToString();

            XmlDocument doc = new XmlDocument();

            XmlElement gettoken = doc.CreateElement("getToken");
            XmlElement item = doc.CreateElement("item");

            XmlElement semilla = doc.CreateElement("Semilla");
            semilla.InnerText = valuesemilla;
            item.AppendChild(semilla);

            gettoken.AppendChild(item);
            doc.AppendChild(gettoken);

            doc.Save("token.xml");

			SignToken objFirma = new SignToken();
			objFirma.signToken("token.xml","token.xml", "", "getToken");
			
			 string contenido = File.ReadAllText("token.xml");
			
			
			
			
			
			
			
			string valortoken = RequestToken(contenido, environment);

            return valortoken;
        }

        public string RequestToken(string  contenido, string environment)
        {
           
			
			
            Console.Write(contenido);

            string direccion = "https://" + environment + "/DTEWS/GetTokenFromSeed.jws?WSDL";

            WebClient client = new WebClient();
            client.Headers.Add("SOAPAction", "getToken");
			client.Headers.Add(HttpRequestHeader.ContentType, "text/xml");
            string inputsoap =
               
				 "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\""+ "\n"+
        "xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\""+"\n"+
        "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""+"\n"+
        "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\""+"\n"+
        "SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">"+"\n"+
        "<SOAP-ENV:Body>"+"\n"+
        "<m:getToken xmlns:m=\"https://"+environment+"/DTEWS/GetTokenFromSeed.jws\">"+"\n"+
        "<pszXml xsi:type=\"xsd:string\">"+"\n"+
        "<![CDATA["+"\n"+contenido+"]]>"+
        "</pszXml>"+"\n"+
        "</m:getToken>"+"\n"+
        "</SOAP-ENV:Body>"+"\n"+
        "</SOAP-ENV:Envelope>";
                
				
				
			Console.WriteLine(inputsoap);
			
            byte[] responseArray = client.UploadData(direccion, "POST", Encoding.UTF8.GetBytes(inputsoap));
            string salida = Encoding.UTF8.GetString(responseArray);


            salida = salida.Replace("&lt;", "<").Replace("&quot;", "\"").Replace("&gt;", ">");

            string original = "<?xml version=" + "\"" + "1.0" + "\"" + " encoding=" + "\"" + "UTF-8" + "\"" + "?>";
            string reemplazo = "";

            salida = salida.Replace(original, reemplazo);

            string valortoken = ReadFileToken(salida);
            return valortoken;
        }

        public string ReadFileToken(string cadena)
        {
			
			Console.WriteLine(cadena);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(cadena);

            XmlNodeList nl = doc.GetElementsByTagName("TOKEN");
            XmlNode el = nl.Item(0);
            string valortoken = el.FirstChild.InnerText;
            return valortoken;
        }
    }
