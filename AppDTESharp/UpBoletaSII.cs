using System;
using System.IO;
using System.Net;
using System.Text;

    public class UpBoletaSII
    {
        public string UpBoleta(string valorToken, string nombreDTE, string rutEmisor, string rutUsuario, string postboleta)
        {
            string[] arrayRutEmisor = rutEmisor.Split('-');
            string[] arrayRutUsuario = rutUsuario.Split('-');


            string url = "https://" + postboleta + "/recursos/v1/boleta.electronica.envio";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
            request.Method = "POST";
            request.ContentType = "multipart/form-data; boundary=9022632e1130lc4";
            request.UserAgent = "Mozilla/4.0 (compatible; PROG 1.0; Windows NT 5.0; YComp 5.0.2.4)";
            request.Headers.Add("Cookie", "TOKEN=" + valorToken);

            string archivo =  nombreDTE;

            using (StreamReader sr = new StreamReader(archivo))
            {
                string contenido = sr.ReadToEnd();

                string stringRequest =
                    "--9022632e1130lc4" + "\r\n" +
                    "Content-Disposition: form-data; name=\"rutSender\"" + "\r\n" +
                    "\r\n" + arrayRutUsuario[0] + "\r\n" +
                    "--9022632e1130lc4" + "\r\n" +
                    "Content-Disposition: form-data; name=\"dvSender\"" + "\r\n" +
                    "\r\n" + arrayRutUsuario[1] + "\r\n" +
                    "--9022632e1130lc4" + "\r\n" +
                    "Content-Disposition: form-data; name=\"rutCompany\"" + "\r\n" +
                    "\r\n" + arrayRutEmisor[0] + "\r\n" +
                    "--9022632e1130lc4" + "\r\n" +
                    "Content-Disposition: form-data; name=\"dvCompany\"" + "\r\n" +
                    "\r\n" + arrayRutEmisor[1] + "\r\n" +
                    "--9022632e1130lc4" + "\r\n" +
                    "Content-Disposition: form-data; name=\"archivo\";filename=\"" + archivo + "\"" + "\r\n" +
                    "Content-Type: application/octet-stream" + "\r\n" +
                    "Content-Transfer-Encoding: binary" + "\r\n" +
                    "\r\n" +
                    contenido + "\r\n" +
                    "\r\n" + "--9022632e1130lc4--";

                Console.Write(stringRequest);

                using (Stream requestStream = request.GetRequestStream())
                using (StreamWriter writer = new StreamWriter(requestStream))
                {
                    writer.Write(stringRequest);
                }

                string targetString = "";
                using (WebResponse response = request.GetResponse())
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    targetString = reader.ReadToEnd();
                }

                Console.Write(targetString);
                return targetString;
            }
        }
    }

