using System;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;

public class SignXML
{
    public void signXML(string xmlFilePath, string outputFilePath, String idDocumento, String nodoDocumento)
    {
        try
        {
            // Cargar el documento XML
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Load(xmlFilePath);

            // Crear un objeto SignedXml
            SignedXml signedXml = new SignedXml(xmlDoc.DocumentElement);

            // Cargar el certificado digital
            X509Certificate2 certificate = LoadCertificate("/home/esteban/appdte/certificate/eguenul.pfx", "amulen1956");

            // Obtener la clave privada
            RSA key = GetRSAKey(certificate);
			
			RSA publicKey = certificate.GetRSAPublicKey();
            RSAKeyValue rsaKeyValue = new RSAKeyValue(publicKey);
			
			

            // Crear una referencia al documento XML que se va a firmar
            Reference reference = new Reference($"#{idDocumento}");
            reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
		    reference.DigestMethod = "http://www.w3.org/2000/09/xmldsig#sha1";
       
            signedXml.AddReference(reference);

            // Crear una firma digital
            KeyInfo keyInfo = new KeyInfo();
           keyInfo.AddClause(rsaKeyValue);
			keyInfo.AddClause(new KeyInfoX509Data(certificate));
			signedXml.KeyInfo = keyInfo;

            // Firmar el documento
            // La firma se crea mediante el proveedor de criptografía RSA específico (RSA o RSAOpenSsl)
            signedXml.SigningKey = key;

            // Crear el objeto SignedInfo
            signedXml.SignedInfo.SignatureMethod = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
            signedXml.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigCanonicalizationUrl;

            // Crear la firma
            signedXml.ComputeSignature();

            // Obtener la representación XML de la firma y asignarla a la variable signatureElement
            XmlElement signatureElement = signedXml.GetXml();

			
			 String StringFirma = FormatXml(signatureElement);
			/*
			
			xmlDoc.DocumentElement.InnerXml += StringFirma;
			*/
			
            // Obtener el nodo Documento del documento original
        /*
			XmlDocument signatureXmlDoc = new XmlDocument();
            signatureXmlDoc.LoadXml(StringFirma);
            XmlNode importedSignature = xmlDoc.ImportNode(signatureXmlDoc.DocumentElement, false);
*/
			
			
			
            xmlDoc.DocumentElement.AppendChild(signatureElement);
        
		  
			
			xmlDoc.InnerXml = xmlDoc.InnerXml.Replace("encoding=\"iso-8859-1\"", "encoding=\"ISO-8859-1\"");


		   xmlDoc.Save(outputFilePath);

                Console.WriteLine($"Documento XML firmado con éxito. Guardado en: {outputFilePath}");
            
	}
        catch (Exception ex)
        {
            Console.WriteLine($"Error al firmar el documento XML: {ex.Message}");
        }
    }

    private X509Certificate2 LoadCertificate(string certificatePath, string password)
    {
        // Cargar el certificado desde el archivo PFX
        return new X509Certificate2(certificatePath, password, X509KeyStorageFlags.Exportable | X509KeyStorageFlags.MachineKeySet);
    }

    private RSA GetRSAKey(X509Certificate2 certificate)
    {
        // Obtener la clave privada utilizando el proveedor de criptografía específico
        if (certificate.PrivateKey is RSA rsaKey)
        {
            return rsaKey;
        }
        else
        {
            throw new Exception("No se pudo obtener la clave privada RSA del certificado.");
        }
    }

	
	private string FormatXml(XmlNode xmlNode)
{
    // Crear un StringWriter con la configuración de formato
    using (StringWriter sw = new StringWriter())
    {
        XmlWriterSettings settings = new XmlWriterSettings
        {
            OmitXmlDeclaration = true,
            Indent = true,
            IndentChars = " ",  // Puedes ajustar esto para cambiar la cantidad de espacios para la indentación
            NewLineChars = "\r\n",
            NewLineHandling = NewLineHandling.Replace
        };

        // Escribir el nodo formateado en el StringWriter
        using (XmlWriter xw = XmlWriter.Create(sw, settings))
        {
            xmlNode.WriteTo(xw);
        }

        // Obtener el nodo formateado como cadena
        return sw.ToString();
    }
}
	
	
	
	
  
}