
public class IdDTE{

private int tipodte;
private int numdte;
private String fechaemision;
private int tipodespacho;
private int indtraslado;
private int frmapago;
private int indservicio;
private int indmntneto;

	public int getTipodte() {
		return this.tipodte;
	}

	public void setTipodte(int tipodte) {
		this.tipodte = tipodte;
	}

	public int getNumdte() {
		return this.numdte;
	}

	public void setNumdte(int numdte) {
		this.numdte = numdte;
	}

	public String getFechaemision() {
		return this.fechaemision;
	}

	public void setFechaemision(String fechaemision) {
		this.fechaemision = fechaemision;
	}

	public int getTipodespacho() {
		return this.tipodespacho;
	}

	public void setTipodespacho(int tipodespacho) {
		this.tipodespacho = tipodespacho;
	}



  public int getIndtraslado()
    {
        return this.indtraslado;
    }

    public void setIndtraslado(int indtraslado)
    {
        this.indtraslado = indtraslado;
    }

	public int getFrmapago() {
		return this.frmapago;
	}

	public void setFrmapago(int frmapago) {
		this.frmapago = frmapago;
	}

	public int getIndservicio() {
		return this.indservicio;
	}

	public void setIndservicio(int indservicio) {
		this.indservicio = indservicio;
	}

	public int getIndmntneto() {
		return this.indmntneto;
	}

	public void setIndmntneto(int indmntneto) {
		this.indmntneto = indmntneto;
	}

}