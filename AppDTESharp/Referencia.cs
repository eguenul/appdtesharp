

public class Referencia{


    private int nrolinref;
    private int tpodocref;
    private int folioref;
    private String fchref;
    private int codref;
    private String razonref;


    public int getNrolinref()
    {
        return this.nrolinref;
    }

    public void setNrolinref(int nrolinref)
    {
        this.nrolinref = nrolinref;
    }

    public int getTpodocref()
    {
        return this.tpodocref;
    }

    public void setTpodocref(int tpodocref)
    {
        this.tpodocref = tpodocref;
    }

    public int getFolioref()
    {
        return this.folioref;
    }

    public void setFolioref(int folioref)
    {
        this.folioref = folioref;
    }

    public String getFchref()
    {
        return this.fchref;
    }

    public void setFchref(String fchref)
    {
        this.fchref = fchref;
    }

    public int getCodref()
    {
        return this.codref;
    }

    public void setCodref(int codref)
    {
        this.codref = codref;
    }

    public String getRazonref()
    {
        return this.razonref;
    }

    public void setRazonref(String razonref)
    {
        this.razonref = razonref;
    }




}