public class DteModel{


private Emisor emisor;
private Receptor receptor;
private Totales totales;
private List<Detalle> detalle;
private IdDTE iddte;
private List<Referencia> referencia;
private List<DscRcgGlobal> dscrcgglobal;
private List<ImptoReten> imptoreten;

public List<ImptoReten> getImptoreten(){

	return this.imptoreten;
}

public void setImptoreten(List<ImptoReten> imptoreten){

	this.imptoreten = imptoreten;
}


    public List<DscRcgGlobal> getDscrcgglobal()
    {
        return this.dscrcgglobal;
    }

    public void setDscrcgglobal(List<DscRcgGlobal> dscrcgglobal)
    {
        this.dscrcgglobal = dscrcgglobal;
    }




    public IdDTE getIddte(){
        return this.iddte;
    }

    public void setIddte(IdDTE iddte){
        this.iddte = iddte;
    }


    public Emisor getEmisor() {
		return this.emisor;
	}

	public void setEmisor(Emisor emisor) {
		this.emisor = emisor;
	}

	public Receptor getReceptor() {
		return this.receptor;
	}

	public void setReceptor(Receptor receptor) {
		this.receptor = receptor;
	}

	public Totales getTotales() {
		return this.totales;
	}

	public void setTotales(Totales totales) {
		this.totales = totales;
	}

	public List<Detalle> getDetalle() {
		return this.detalle;
	}

	public void setDetalle(List<Detalle> detalle) {
		this.detalle = detalle;
	}



 public List<Referencia> getReferencia()
    {
        return this.referencia;
    }

    public void setReferencia(List<Referencia> referencia)
    {
        this.referencia = referencia;
    }




}