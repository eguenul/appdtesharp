public class Receptor{


   String rutreceptor;
   String rsreceptor;
   String giroreceptor;
   String dirreceptor;
   String cmnareceptor;
   String ciureceptor;



	public String getRutreceptor() {
		return this.rutreceptor;
	}

	public void setRutreceptor(String rutreceptor) {
		this.rutreceptor = rutreceptor;
	}

	public String getRsreceptor() {
		return this.rsreceptor;
	}

	public void setRsreceptor(String rsreceptor) {
		this.rsreceptor = rsreceptor;
	}

	public String getGiroreceptor() {
		return this.giroreceptor;
	}

	public void setGiroreceptor(String giroreceptor) {
		this.giroreceptor = giroreceptor;
	}

	public String getDirreceptor() {
		return this.dirreceptor;
	}

	public void setDirreceptor(String dirreceptor) {
		this.dirreceptor = dirreceptor;
	}

	public String getCmnareceptor() {
		return this.cmnareceptor;
	}

	public void setCmnareceptor(String cmnareceptor) {
		this.cmnareceptor = cmnareceptor;
	}

	public String getCiureceptor() {
		return this.ciureceptor;
	}

	public void setCiureceptor(String ciureceptor) {
		this.ciureceptor = ciureceptor;
	}
}