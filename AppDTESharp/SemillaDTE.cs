using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

public class SemillaDTE
    {
	
	 public SemillaDTE(){
		 
	 } 
	
	
        public string GetSeed(string environment)
        {
            try
            {
               
                string direccion = $"https://{environment}/DTEWS/CrSeed.jws?WSDL";

                WebRequest request = WebRequest.Create(direccion);
                request.Method = "POST";
                request.ContentType = "text/xml; charset=utf-8";
                request.Headers.Add("SOAPAction", "getSeed");

                string inputSoap = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                                    "<soapenv:Header/>" +
                                    "<soapenv:Body>" +
                                    "<getSeed></getSeed>" +
                                    "</soapenv:Body>" +
                                    "</soapenv:Envelope>";

                byte[] buffer = Encoding.UTF8.GetBytes(inputSoap);
                request.ContentLength = buffer.Length;

                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(buffer, 0, buffer.Length);
                }

                using (WebResponse response = request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string output = reader.ReadToEnd();
					
                    output = output.Replace("&lt;", "<").Replace("&quot;", "\"").Replace("&gt;", ">");

                    string original = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                    string replacement = "";
                    output = output.Replace(original, replacement);
                   
                    Console.Write(output);

                    string seedValue = ReadSeed(output);
                    return seedValue;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return "0";
            }
        }

        public string ReadSeed(string request)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(request);

                XmlNodeList nodeList = doc.GetElementsByTagName("SEMILLA");
                XmlNode element = nodeList.Item(0);
                string seedValue = element.FirstChild?.InnerText;

                return seedValue;
            }
            catch (Exception)
            {
                return "0";
            }
        }
    }