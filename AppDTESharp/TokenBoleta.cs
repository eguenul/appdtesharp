using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Net;

public class TokenBoleta
{
  
	 public string GetToken(string valorsemilla, string pathcertificado, string clave, string nombredte, string environment)
        {
           
            XmlDocument doc = new XmlDocument();

            XmlElement gettoken = doc.CreateElement("getToken");
            XmlElement item = doc.CreateElement("item");

            XmlElement semilla = doc.CreateElement("Semilla");
            semilla.InnerText = valorsemilla;
            item.AppendChild(semilla);

            gettoken.AppendChild(item);
            doc.AppendChild(gettoken);

            doc.Save("token.xml");

			SignToken objFirma = new SignToken();
			objFirma.signToken("token.xml","token.xml", "", "getToken");
			
			 string contenido = File.ReadAllText("token.xml");
			
			
			
			
			
			
			
			string valortoken = RequestToken(contenido, environment);

            return valortoken;
        }
	
	
	
	public string RequestToken(string xmlContent, string environment)
        {
            
		string apiUrl = "https://"+environment+"/recursos/v1/boleta.electronica.token";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = "POST";
            request.Accept = "application/xml";
            request.ContentType = "application/xml";

            using (Stream requestStream = request.GetRequestStream())
            {
                using (StreamWriter streamWriter = new StreamWriter(requestStream))
                {
                    streamWriter.Write(xmlContent);
                }
            }

            string targetString;
            using (WebResponse response = request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8))
                    {
                        targetString = streamReader.ReadToEnd();
                    }
                }
            }

            return ReadFileToken(targetString);
        }
	
	
	
	
	

    public string ReadFileToken(string cadena)
    {
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(cadena);
        XmlNodeList nl = doc.GetElementsByTagName("TOKEN");
        XmlNode el = nl.Item(0);
        string valorToken = el.FirstChild.InnerText;
		Console.WriteLine(valorToken);
        return valorToken;
    }
}